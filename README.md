# miniCAD
A very simple 2D CAD program.

Originally based on https://www.youtube.com/watch?v=kxKKHKSMGIg. But this version includes some improvements.

## Usage
- `Mouse wheel button`. Panning
- `Mouse wheel`. Zooming
- `L` key. Line
  - Defines a line using two points. The first point is placed at the pointer position when the key was pressed. The
    second point is defined using the primary mouse button.
- `C` key. Circle
  - Defines a circle using two points. The first point is placed at the pointer position when the key was pressed. The
  second point is defined using the primary mouse button.
- `B` key. Box (square/rectangle)
  - Defines a box using two points. The first point is placed at the pointer position when the key was pressed. The
    second point is defined using the primary mouse button.
- `M` key. Move a construction point of a shape.
  - Enters the move state. With the mouse select a point using the primary button, hold it down and drag the point moving
  the pointer. Once you release the button, the application gets out of this state.

## Dependencies

### Linux

**TL;DR** Install the dependencies with the command
```shell
sudo apt install libpng-dev libglu1-mesa-dev
```

#### olcPixelGameEngine
Already included as part of the project. It's only one header file located in the `third-party` directory.
But this library has other dependencies listed here:
- libpng (libpng-dev)
- OpenGL 1.0 (libglu1-mesa-dev)
- X11 (libx11-dev). It should be installed by default.

## Notes
- Building instructions for an executable using olcPixelGameEngine: https://github.com/OneLoneCoder/olcPixelGameEngine/wiki/Compiling-on-Linux

## TODO
- Implement the Curve or Bezier tool.
- Make the tools to behave like my `Move` tool version. This is, the app enters a state, and while in this state, perform
the actions with mouse clicks.
- Include a visual aide to reflect in what state you are.
  - Maybe create something like a status bar, where it displays the pointer position and the current app state

### Infrastructure
- Extract the shape creation from the method `MiniCadApp.OnUserUpdate()`.