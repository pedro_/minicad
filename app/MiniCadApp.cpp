#define OLC_PGE_APPLICATION

#include "MiniCadApp.h"

#include <cmath>

#include "third-party/olcPixelGameEngine.h"

#include "shape/Box.h"
#include "shape/Circle.h"
#include "shape/Line.h"
#include "shape/Node.h"


namespace minicad {

    MiniCadApp::MiniCadApp() {
        sAppName = "MiniCadApp";
    }

    // MiniCadApp::~MiniCadApp() {
    //     for (auto s : shapesList) {
    //         std::cout << "Deleting shape: " << s << "\n";
    //         delete s;
    //     }
    // }
    MiniCadApp::~MiniCadApp() = default;

    bool MiniCadApp::OnUserCreate() {
        // Called once at the start, so create things here

        // Default offset in World Space is the middle of the screen
        vOffset = {
            -static_cast<float>(ScreenWidth()) * 0.5f / fScale,
            -static_cast<float>(ScreenHeight()) * 0.5f / fScale
        };

        computeVisibleWorld();

        return true;
    }

    bool MiniCadApp::OnUserUpdate(float fElapsedTime) {
        // called once per frame
        olc::vf2d vMouse{static_cast<float>(GetMouseX()), static_cast<float>(GetMouseY())};

        // Processs PANNING //
        // NOTE: `bPressed` is not detected when bHeld is active, that's why we need to have two separate `if` statements
        // Panning. Start when mouse's SCROLL BUTTON is pressed
        if (GetMouse(2).bPressed) {
            vStartPan = vMouse;
        }

        // Compute the panning if the mouse's SCROLL BUTTON is held down and moved around
        if (GetMouse(2).bHeld) {
            vOffset -= (vMouse - vStartPan) / fScale;
            // Update the vStartPan given that `bPressed` is not detected when `bHeld` is active
            vStartPan = vMouse;
        }
        // //

        // Process ZOOMING //
        // Zooming involves getting the mouse position at the start of the process, then update the scale, and computing
        // the new position of the mouse. With this, we update the new offset from the Origin of the World space

        // Zoooming can be performed with the mouse wheel or with Q and A keys
        if (GetKey(olc::Key::Q).bPressed || GetMouseWheel() > 0) {
            olc::vf2d vMouseBeforeZoom;
            screenToWorld(static_cast<int>(vMouse.x), static_cast<int>(vMouse.y), vMouseBeforeZoom);
            fScale *= 1.1f; // 10% increments
            // Compute the new position of the mouse once the scale has changed and translate the world to that position
            olc::vf2d vMouseAfterZoom;
            screenToWorld(static_cast<int>(vMouse.x), static_cast<int>(vMouse.y), vMouseAfterZoom);
            vOffset += (vMouseBeforeZoom - vMouseAfterZoom);
            // vOffset += (vMouseAfterZoom - vMouseBeforeZoom);
        }
        else if (GetKey(olc::Key::A).bPressed || GetMouseWheel() < 0) {
            olc::vf2d vMouseBeforeZoom;
            screenToWorld(static_cast<int>(vMouse.x), static_cast<int>(vMouse.y), vMouseBeforeZoom);
            fScale *= 0.9f; // 10% decrements
            // Compute the new position of the mouse once the scale has changed and translate the world to that position
            olc::vf2d vMouseAfterZoom;
            screenToWorld(static_cast<int>(vMouse.x), static_cast<int>(vMouse.y), vMouseAfterZoom);
            vOffset += (vMouseBeforeZoom - vMouseAfterZoom);
            // vOffset += (vMouseAfterZoom - vMouseBeforeZoom);
        }
        // //

        // Compute cursor //
        // Snaps to the nearest grid point
        screenToWorld(static_cast<int>(vMouse.x), static_cast<int>(vMouse.y), vMouseWorld);
        vCursor.x = std::floor((vMouseWorld.x + 0.5f) * fGrid);
        vCursor.y = std::floor((vMouseWorld.y + 0.5f) * fGrid);
        // //


        if (GetKey(olc::Key::ESCAPE).bPressed) {
            // Cancel current action
            currentCommand = CommandNone;
            std::cout << "None pressed\n";
        } else if (GetKey(olc::Key::C).bPressed) {
            // Move circle
            currentCommand = CommandCircle;
            std::cout << "C pressed\n";

            if (tmpShape == nullptr) {
                // tmpShape = new Circle(*this);
                tmpShape = std::make_unique<Circle>(*this);

                // Place first node at location of keypress
                selectedNode = tmpShape->getNextNode(vCursor);
                std::cout << "Node at " << selectedNode->getPosition().x << ", " << selectedNode->getPosition().y << "\n";

                // Immediately create the second node. But this one will be updated in the `if` block below with the cursor
                // movement
                // NOTE: this is tricky, I don't think it's a good design
                selectedNode = tmpShape->getNextNode(vCursor);
                std::cout << "Node at " << selectedNode->getPosition().x << ", " << selectedNode->getPosition().y << "\n";
            }
        } else if (GetKey(olc::Key::M).bPressed) {
            // Move node
            currentCommand = CommandMovePoint;
            std::cout << "M pressed\n";
        } else if (GetKey(olc::Key::P).bPressed) {
            // Create point
            std::cout << "P pressed\n";
            currentCommand = CommandPoint;
        } else if (GetKey(olc::Key::L).bPressed) {
            // Create line
            std::cout << "L pressed\n";
            currentCommand = CommandLine;

            if (tmpShape == nullptr) {
                // tmpShape = new Line(*this);
                tmpShape = std::make_unique<Line>(*this);

                // Place first node at location of keypress
                selectedNode = tmpShape->getNextNode(vCursor);
                std::cout << "Node at " << selectedNode->getPosition().x << ", " << selectedNode->getPosition().y << "\n";

                // Immediately create the second node. But this one will be updated in the `if` block below with the cursor
                // movement
                // NOTE: this is tricky, I don't think it's a good design
                selectedNode = tmpShape->getNextNode(vCursor);
                std::cout << "Node at " << selectedNode->getPosition().x << ", " << selectedNode->getPosition().y << "\n";
            }
        } else if (GetKey(olc::Key::B).bPressed) {
            // Create a rectangle
            std::cout << "B pressed\n";
            currentCommand = CommandLine;

            if (tmpShape == nullptr) {
                // tmpShape = new Box(*this);
                tmpShape = std::make_unique<Box>(*this);

                // Place first node at location of keypress
                selectedNode = tmpShape->getNextNode(vCursor);
                std::cout << "Node at " << selectedNode->getPosition().x << ", " << selectedNode->getPosition().y << "\n";

                // Immediately create the second node. But this one will be updated in the `if` block below with the cursor
                // movement
                // NOTE: this is tricky, I don't think it's a good design
                selectedNode = tmpShape->getNextNode(vCursor);
                std::cout << "Node at " << selectedNode->getPosition().x << ", " << selectedNode->getPosition().y << "\n";
            }
        }

        // Update the last node of the line
        // NOTE: this is tricky, I don't think it's a good design
        if (selectedNode != nullptr) {
            // std::cout << "Updating node\n";
            selectedNode->setPosition(vCursor);
        }

        // Fix the last node of the line with `getNextNode()`. After this, it will return nullptr if called again
        // NOTE: this is tricky, I don't think it's a good design
        if ((tmpShape != nullptr) && GetMouse(0).bReleased) {
            std::cout << "Construct line\n";
            selectedNode = tmpShape->getNextNode(vCursor);
            if (selectedNode == nullptr) {
                tmpShape->setColor(olc::WHITE);
            }
            // shapesList.push_back(tmpShape);
            shapesList.push_back(std::move(tmpShape));
            tmpShape = nullptr;
        }

        switch (currentCommand) {
            case CommandMovePoint: {
                if (GetMouse(0).bPressed) {
                    // Find the nearest node to the cursor in all the shapes of the shapes list
                    for (const auto& s : shapesList) {
                        if (s == nullptr) {
                            continue;
                        }
                        selectedNode = s->getNodeUnderPos(vCursor);
                        if (selectedNode != nullptr) {
                            break;
                        }
                    }
                }

                if ((selectedNode != nullptr) && GetMouse(0).bReleased) {
                    // Revert command. The modification will be handled by the default behavior in the next rendering
                    // iteration
                    selectedNode = nullptr;
                    currentCommand = CommandNone;
                    std::cout << "Move finished\n";
                }
            }

        }

        // Clear screen
        Clear(olc::VERY_DARK_RED);

        // Drawing //
        computeVisibleWorld();
        drawGrid();
        drawAxes(dashPattern4);
        drawCursor();

        // for (const auto s : shapesList) {
        //     if (s != nullptr) {
        //         s->draw();
        //     }
        // }
        for (auto& s : shapesList) {
            if (s != nullptr) {
                s->draw();
            }
        }
        if (tmpShape != nullptr) {
            tmpShape->draw();
        }
        // //


        // Testing. Generate random noise
        // for (int x = 0; x < ScreenWidth(); x++)
        // 	for (int y = 0; y < ScreenHeight(); y++)
        // 		Draw(x, y, olc::Pixel(rand() % 255, rand() % 255, rand()% 255));
        //

        return true;
    }

    float MiniCadApp::getScale() const {
        return fScale;
    }



    // ------------------------------------------------------------------------------------------------------------------ //
    //  PRIVATE member functions
    // ------------------------------------------------------------------------------------------------------------------ //
    // Zooming and panning are all about transforming from world space to screen space
    // Functions Convert coordinates from World Space to Screen Space and vice-versa
    //  input: v
    //  output: screenX, screenY
    void MiniCadApp::worldToScreen(const olc::vf2d& v, int& screenX, int& screenY) const {
        screenX = static_cast<int>((v.x - vOffset.x) * fScale);
        screenY = static_cast<int>((v.y - vOffset.y) * fScale);
    }

    //  input: screenX,  screenY
    //  output: v
    void MiniCadApp::screenToWorld(int screenX, int screenY, olc::vf2d& v) const {
        v.x = static_cast<float>(screenX) / fScale + vOffset.x;
        v.y = static_cast<float>(screenY) / fScale + vOffset.y;
    }
    //

    // ---------------------------------------------------------
    //  Drawing member functions
    // ---------------------------------------------------------

    void MiniCadApp::computeVisibleWorld() {
        // Compute visible portion of the World Space in World Space coordinates
        screenToWorld(0, 0, vWorldTopLeft);
        screenToWorld(ScreenWidth(), ScreenHeight(), vWorldBottomRight);

        // Avoid clipping of pixels rendered at the edges
        vWorldTopLeft.x = floor(vWorldTopLeft.x);
        vWorldTopLeft.y = floor(vWorldTopLeft.y);
        vWorldBottomRight.x = ceil(vWorldBottomRight.x);
        vWorldBottomRight.y = ceil(vWorldBottomRight.y);
    }

    void MiniCadApp::drawGrid() {
        int sx, sy;

        // Iterate in world space, then compute the values of that coordinate into Screen Space and draw it.
        //  NOTE: This seems a bit expensive, we draw each point of the grid and we create an `vf2d` object in each step
        for (int x = static_cast<int>(vWorldTopLeft.x); x < static_cast<int>(vWorldBottomRight.x);
             x += static_cast<int>(fGrid)) {
            for (int y = static_cast<int>(vWorldTopLeft.y); y < static_cast<int>(vWorldBottomRight.y);
                 y += static_cast<int>(fGrid)) {
                worldToScreen({static_cast<float>(x), static_cast<float>(y)}, sx, sy);
                Draw(sx, sy, olc::RED);
                 }
             }
    }

    void MiniCadApp::drawAxes(uint32_t pattern) {
        int sx, sy;
        int ex, ey;

        // y axis
        worldToScreen({0, vWorldTopLeft.y}, sx, sy);
        worldToScreen({0, vWorldBottomRight.y}, ex, ey);
        DrawLine(sx, sy, ex, ey, olc::GREY, pattern);

        // x axis
        worldToScreen({vWorldTopLeft.x, 0}, sx, sy);
        worldToScreen({vWorldBottomRight.x, 0}, ex, ey);
        DrawLine(sx, sy, ex, ey, olc::GREY, pattern);
        //
    }

    void MiniCadApp::drawCursor() {
        int sx, sy;

        worldToScreen(vCursor, sx, sy);
        DrawCircle(sx, sy, 3, olc::YELLOW);
        DrawString(5, 5,
                   "Cursor=(" + std::to_string(vCursor.x) + ", " + std::to_string(vCursor.y) + ")",
                   olc::YELLOW);
    }
}
// ------------------------------------------------------------------------------------------------------------------ //
