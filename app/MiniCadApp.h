//
// Created by hg on 2/15/24.
//

#ifndef MINICADAPP_H
#define MINICADAPP_H

#include "CommonFwd.h"

#include "third-party/olcPixelGameEngine.h"

#include "shape/IShape.h"
#include "shape/Node.h"


namespace minicad {
    // TODO: Move this to a header file
    // The engine uses some weird method to set the pattern. Javid mentions "pixels" but I'm not sure they are pixels
    constexpr uint32_t dashPattern1 = 0xAAAAAAAA; // 1 pixel on, 1 pixel off
    constexpr uint32_t dashPattern2 = 0xCCCCCCCC; // 2 pixels on, 2 pixels off
    constexpr uint32_t dashPattern4 = 0xF0F0F0F0; // 4 pixels on, 4 pixels off
    constexpr uint32_t dashPattern8 = 0xFF00FF00; // 8 pixels on, 8 pixels off
    constexpr uint32_t dashPattern16 = 0xFFFF0000; // 16 pixels on, 16 pixels off
    //


    enum AppCommand {
        CommandNone,
        CommandMovePoint,
        CommandPoint,
        CommandLine,
        CommandCircle
    };

    class MiniCadApp : public olc::PixelGameEngine {
    public:
        MiniCadApp();

        ~MiniCadApp() override;

        bool OnUserCreate() override;

        bool OnUserUpdate(float fElapsedTime) override;

        //  input: v | output: screenX, screenY
        void worldToScreen(const olc::vf2d& v, int& screenX, int& screenY) const;
        float getScale() const;

        AppCommand currentCommand{CommandNone};

    private:
        //  input: screenX,  screenY | output: v
        void screenToWorld(int screenX, int screenY, olc::vf2d& v) const;

        //  Some drawing member function //
        void computeVisibleWorld();

        void drawGrid();

        void drawAxes(uint32_t pattern = 0xF0F0F0F0);

        void drawCursor();

        // //


        // Member variable for panning and zooming
        olc::vf2d vStartPan{0.f, 0.f};

        olc::vf2d vOffset{0.f, 0.f}; // Offset from the World space origin to the Screen center in World space units
        float fScale{10.0f}; //	Scale between World space and Screen space
        float fGrid{1.0f}; // World Space step (spacing between points in World Space)
        olc::vf2d vCursor{0.0f, 0.0f}; // Representation of the cursor in the World. It snaps to the closest grid point
        olc::vf2d vMouseWorld{0.0f, 0.0f};

        // Visible portion of the World Space in World Space coordinates
        olc::vf2d vWorldTopLeft;
        olc::vf2d vWorldBottomRight;

        // Temp. Just to test object creation
        // IShape* tmpShape{nullptr};
        // std::list<IShape *> shapesList{};
        std::unique_ptr<IShape> tmpShape;
        std::list<std::unique_ptr<IShape>> shapesList;

        Node* selectedNode{nullptr};
        //
    };
}

#endif //MINICADAPP_H
