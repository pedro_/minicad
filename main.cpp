#include <iostream>

#include "app/MiniCadApp.h"


int main() {
    if (minicad::MiniCadApp app; app.Construct(800, 600, 1, 1)) {
        app.Start();
    }


    return 0;
}
