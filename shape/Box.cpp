//
// Created by hg on 2/17/24.
//

#include "Box.h"

#include "app/MiniCadApp.h"


namespace minicad {
    Box::Box(MiniCadApp& app) : IShape{app} {
        maxNodes_ = 2;
        nodes_.reserve(maxNodes_);
    }

    void Box::draw() {
        int x1, y1;
        int x2, y2;
        app_.worldToScreen(nodes_[0].getPosition(), x1, y1);
        app_.worldToScreen(nodes_[1].getPosition(), x2, y2);
        app_.DrawRect(x1, y1, x2 - x1, y2 - y1, color_);
        drawNodes();
    }
} // minicad
