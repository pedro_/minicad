//
// Created by hg on 2/17/24.
//

#ifndef BOX_H
#define BOX_H
#include "IShape.h"


namespace minicad {
    class Box : public IShape {
    public:
        explicit Box(MiniCadApp& app);

        void draw() override;
    };
} // minicad

#endif //BOX_H
