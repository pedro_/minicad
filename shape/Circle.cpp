//
// Created by hg on 2/17/24.
//

#include "Circle.h"

#include "app/MiniCadApp.h"


namespace minicad {
    Circle::Circle(MiniCadApp& app) : IShape{app} {
        maxNodes_ = 2;
        nodes_.reserve(maxNodes_);
    }

    void Circle::draw() {
        int32_t x1, y1;
        int32_t x2, y2;
        int32_t radius = static_cast<int32_t>(app_.getScale() * (nodes_[1].getPosition() - nodes_[0].getPosition()).mag());

        app_.worldToScreen(nodes_[0].getPosition(), x1, y1);
        app_.worldToScreen(nodes_[1].getPosition(), x2, y2);

        app_.DrawCircle(x1, y1, radius, color_);
        // Draw the radius line
        app_.DrawLine(x1, y1, x2, y2, olc::WHITE, dashPattern2);
        drawNodes();
    }


}
