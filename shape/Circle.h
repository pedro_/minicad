//
// Created by hg on 2/17/24.
//

#ifndef CIRCLE_H
#define CIRCLE_H

#include "IShape.h"

namespace minicad {
    class Circle : public IShape {
    public:
        explicit Circle(MiniCadApp& app);

        void draw() override;
    };
}



#endif //CIRCLE_H
