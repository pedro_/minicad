//
// Created by hg on 2/15/24.
//

#include "IShape.h"

#include "app/MiniCadApp.h"


namespace minicad {
    IShape::IShape(MiniCadApp& app) : app_{app} {
    }

    IShape::~IShape() = default;


    Node* IShape::getNextNode(const olc::vf2d& pos) {
        if (nodes_.size() == maxNodes_) {
            std::cout << "Shape completed\n";
            // Shape is complete. No new nodes to be issued
            return nullptr;
        }

        std::cout << "Adding node\n";
        // Create new node and add it to the shape's vector
        Node node{*this, pos};
        nodes_.push_back(node);

        // Return a pointer to the node we just created
        // NOTE: This is not recommended but we can do it because `nodes_` MUST be declared as a non-rearrangeable
        // vector by calling `reserve(maxNodes_)` method in the constructor of the implementations of IShape
        return &nodes_[nodes_.size() - 1];
    }

    void IShape::drawNodes() {
        for (auto& n: nodes_) {
            int32_t sx, sy;
            app_.worldToScreen(n.getPosition(), sx, sy);
            app_.FillCircle(sx, sy, nodePixelSize_, color_);
        }
    }

    void IShape::setColor(const olc::Pixel& color) {
        color_ = color;
    }

    std::vector<Node>& IShape::getNodes() {
        return nodes_;
    }

    Node* IShape::getNodeUnderPos(const olc::vf2d& p) {
        for (auto& n: nodes_) {
            if ((n.getPosition() - p).mag() < 0.01f) {
                std::cout << "Node found\n";
                return &n;
            }
        }
        return nullptr;
    }
} // minicad
