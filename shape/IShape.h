//
// Created by hg on 2/15/24.
//

#ifndef ISHAPE_H
#define ISHAPE_H

#include "CommonFwd.h"

#include <vector>

#include "third-party/olcPixelGameEngine.h"


namespace minicad {
    class IShape {
    public:
        explicit IShape(MiniCadApp& app);

        virtual ~IShape();

        virtual void draw() = 0;

        // NOTE: consider changing the name `getNextNode`. This function creates a `Node` object from the passed `pos`
        Node* getNextNode(const olc::vf2d& pos);

        Node* getNodeUnderPos(const olc::vf2d& p);

        void setColor(const olc::Pixel& color);

        std::vector<Node>& getNodes();

    protected:
        void drawNodes();

        MiniCadApp& app_;
        uint32_t maxNodes_{0};
        std::vector<Node> nodes_;
        olc::Pixel color_{olc::GREEN};
        const int32_t nodePixelSize_{2};

    private:
        // void drawNodes();
    };
} // minicad

#endif //ISHAPE_H
