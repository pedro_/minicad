//
// Created by hg on 2/15/24.
//

#include "Line.h"

#include "IShape.h"
#include "app/MiniCadApp.h"


namespace minicad {
    Line::Line(MiniCadApp& app) : IShape{app}{
        maxNodes_ = 2;
        nodes_.reserve(maxNodes_);
    }

    void Line::draw() {
        int x1, y1;
        int x2, y2;
        app_.worldToScreen(nodes_[0].getPosition(), x1, y1);
        app_.worldToScreen(nodes_[1].getPosition(), x2, y2);
        app_.DrawLine(x1, y1, x2, y2, color_);
        drawNodes();
    }
} // minicad
