//
// Created by hg on 2/15/24.
//

#ifndef LINE_H
#define LINE_H

#include "CommonFwd.h"

#include "IShape.h"
#include "app/MiniCadApp.h"


namespace minicad {
    class Line : public IShape {
    public:
        explicit Line(MiniCadApp& app);

        void draw() override;
    };
} // minicad

#endif //LINE_H
