//
// Created by hg on 2/15/24.
//

#include "Node.h"

#include "IShape.h"


namespace minicad {
    Node::Node(IShape& parentShape) : parentShape_{parentShape} {
    }

    Node::Node(IShape& parentShape, const olc::vf2d& pos) : Node(parentShape) {
        pos_ = pos;
    }

    Node::Node(IShape& parentShape, float posX, float posY) : Node(parentShape) {
        pos_ = {posX, posY};
    }

    IShape& Node::getParentShape() {
        return parentShape_;
    }

    olc::vf2d& Node::getPosition() {
        return pos_;
    }

    void Node::setPosition(olc::vf2d& pos) {
        pos_ = pos;
    }
} // minicad
