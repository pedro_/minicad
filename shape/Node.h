//
// Created by hg on 2/15/24.
//

#ifndef NODE_H
#define NODE_H

#include "CommonFwd.h"

#include "third-party/olcPixelGameEngine.h"

#include "IShape.h"


namespace minicad {

    class Node {
    public:
        explicit Node(IShape& parentShape);

        Node(IShape& parentShape, const olc::vf2d& pos);

        Node(IShape& parentShape, float posX, float posY);

        // Getters and setters
        IShape& getParentShape();

        olc::vf2d& getPosition();

        void setPosition(olc::vf2d& pos);

    private:
        IShape& parentShape_;
        olc::vf2d pos_{0.f, 0.f};
    };
} // minicad

#endif //NODE_H
